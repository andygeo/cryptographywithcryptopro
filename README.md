# Курсовая работа

Реализация приложения на языке Java, написанного с использованием криптопровайдера КриптоПро, для курсовой работы по курсу "Криптографические протоколы" (осенний семестр 2018/2019 СПбГЭТУ).
Реализовано:

* зашифровывание/расшифровывание файла;

* формирование/проверка электронной подписи.

студенты группы 4362

Георгица Андрей

Тищенко Виталия

* * *


Cryptoapplication in Java using CryptoPro CSP (https://www.cryptopro.ru/) for course project of сryptographic protocols course (fall semester of 2018/2019 academic year, Saint-Petersburg ETU)

Done:

* file encryption/decryption;

* digital signature generation/verification.

Andrey Georgitsa

Tishchenko Vitaliya

* * *