package CryptoOps;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

import static CryptoOps.Signer.*;

public class ConsoleApp {
    private static void skip(){
        System.out.println("====================================");
    }

    public static void signFile() throws Exception{
        Scanner in = new Scanner(System.in);
        System.out.println("FILE SIGNING\nEnter file name:");
        String fileName=in.next();
        if (!new File(fileName).exists()){
            System.out.println("File doesn't exist!");
            return;
        }
        Signer signer=new Signer(SIGN_MODE_NO_GEN);
        System.out.print("Load KeyPair:\nEnter name: ");
        String alias = in.next();
        System.out.print("Enter pass: ");
        char[] password = in.next().toCharArray();
        signer.loadKeyPair(alias, password);
        System.out.println("Sign is generated:\n" + toHexString(signer.signFile(fileName)));
        System.in.read();
    }

    public static void verifySign() throws Exception{
        Scanner in = new Scanner(System.in);
        System.out.println("SIGNATURE VERIFICATION");
        Signer signer=new Signer(SIGN_MODE_NO_GEN);
        System.out.print("Load KeyPair:\nEnter name: ");
        String alias = in.next();
        System.out.print("Enter pass: ");
        char[] password = in.next().toCharArray();
        signer.loadKeyPair(alias, password);
        System.out.print("Enter file name: ");
        String fileName=in.next();
        if (!new File(fileName).exists()){
            System.out.println("File doesn't exist!");
            return;
        }
        System.out.print("Input signature in HEX-format or file path to the signature file: ");
        String inputFileName=in.next();
        File inputFile=new File(inputFileName);
        byte[] signature;
        if (inputFile.exists()){
            String contents = new String(Files.readAllBytes(Paths.get(inputFileName)));
            signature= toByteArr(contents);
            System.out.println("Signature: "+toHexString(signature));
        }else{
            signature=toByteArr(inputFileName);
        }
        if (signer.verifyFile(fileName,signature)){
            System.out.println("Signature is correct!");
        } else{
            System.out.println("Signature is incorrect!");
        }
        System.in.read();
    }

    public static void encryptFile() throws Exception{
        Scanner in = new Scanner(System.in);
        System.out.print("FILE ENCRYPTION\nEnter file name: ");
        String fileName=in.next();
        if (!new File(fileName).exists()){
            System.out.println("File doesn't exist!");
            return;
        }
        Cryptor cryptor=new Cryptor();
        System.out.print("Key phrase: ");
        byte[] password = in.next().getBytes();
        cryptor.encryptFile(fileName,fileName+".crpt",password);
        System.out.println("Encrypted!");
        System.in.read();
        skip();
    }

    public static void decryptFile() throws Exception{
        Scanner in = new Scanner(System.in);
        System.out.print("FILE DECRYPTION\nEnter file name: ");
        String fileName=in.next();
        if (!new File(fileName).exists()){
            System.out.println("File doesn't exist!");
            return;
        }
        System.out.print("Enter destination file name (will be stored in the same directory): ");
        StringBuilder destFile=new StringBuilder();
        destFile.append(new File(fileName).getParent());
        destFile.append(in.next());
        Cryptor cryptor=new Cryptor();
        System.out.print("Key phrase: ");
        byte[] password = in.next().getBytes();
        cryptor.decryptFile(fileName,destFile.toString(),password);
        System.out.println("Decrypted!");
        System.in.read();
        skip();
    }

    public static void generateContainers() throws Exception{
        Scanner in = new Scanner(System.in);

            System.out.println("\nSELECT SIGN MODE\n" +
                        "1. Generate sign with 256 bits keys\n" +
                        "2. Generate sign with 512 bits keys\n");
            char answer=in.next().charAt(0);
            Signer signer;
            switch (answer){
                case '1':
                    signer=new Signer(SIGN_MODE_256);
                    break;
                case '2':
                    signer=new Signer(SIGN_MODE_512);
                    break;
                default:
                    System.out.println("Generating is cancelled!");
                    return;
            }
            System.out.print("Save KeyPair:\nEnter name: ");
            String alias = in.next();
            System.out.print("Enter pass: ");
            char[] password = in.next().toCharArray();
            signer.saveKeyPair(alias,password);
            System.out.println("Saved!");
            System.in.read();

    }

    public static void main(String[] args) throws Exception{
        Scanner in = new Scanner(System.in);
        do{
            System.out.println("Select operation:\n" +
                    "1. Encrypt file\n" +
                    "2. Decrypt file\n" +
                    "3. Sign file\n" +
                    "4. Verify sign\n" +
                    "5. Generate containers\n" +
                    "0. Exit\n");
            char answer=in.next().charAt(0);
            skip();
            switch (answer){
                case '1':
                    try{
                        encryptFile();
                    }catch (Exception ex){
                        System.out.println("Error during encryption process!");
                        ex.printStackTrace();
                    }
                    break;
                case '2':
                    try{
                        decryptFile();
                    }catch (Exception ex){
                        System.out.println("Error during decryption process!");
                        ex.printStackTrace();
                    }

                    break;
                case '3':
                    try{
                        signFile();
                    }catch (Exception ex){
                        System.out.println("Error during signing process!");
                        ex.printStackTrace();
                    }
                    break;
                case '4':
                    try{
                        verifySign();
                    }catch (Exception ex){
                        System.out.println("Error during verification process!");
                        ex.printStackTrace();
                    }
                    break;
                case '5':
                    try{
                        generateContainers();
                    }catch (Exception ex){
                        System.out.println("Error during container generation!");
                        ex.printStackTrace();
                    }
                    break;
                default:
                    return;
            }

        } while (true);
    }
}
