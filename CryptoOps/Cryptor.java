package CryptoOps;

import ru.CryptoPro.Crypto.Key.GostSecretKey;
import ru.CryptoPro.JCP.params.CryptParamsSpec;
import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.*;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import java.security.MessageDigest;
import java.util.Arrays;

public class Cryptor {

    /**
     * Symmetric key for GOST-28147
     */
    private SecretKey сryptKey;

    /**
     * Initialization vector
     */
    private byte[] IV=new byte[8];

    /**
     * Cipher mode
     */
    private static String CIPHER_ALG = "GOST28147/CFB/NoPadding";

    /**
     * Cipher object for encryption and decryption
     */
    Cipher cipher;

    public Cryptor(){
        try{
            cipher = Cipher.getInstance(CIPHER_ALG, "Crypto");
        }catch (NoSuchAlgorithmException ex1) {
            System.out.println(11);
        } catch (NoSuchProviderException ex2) {
            System.out.println(12);
        }catch (NoSuchPaddingException ex3) {
            System.out.println(13);
        }catch (ArrayIndexOutOfBoundsException ex8){
            System.out.println(18);
        }
    }

    /**
     * Set new initialization vector
     * @param data byte array, containing IV
     * @throws IllegalArgumentException/ IV size isn't correct
     */
    public void setIV(byte[] data)throws IllegalArgumentException {
        if(data.length!=8) throw new IllegalArgumentException("Wrong IV size!");
        this.IV=data;
    }

    /**
     * Encryption operation
     * @param data - data to be encrypted
     * @return encrypted bytes
     * @throws Exception/ key or padding are invalid
     */
    public byte[] encrypt(byte[] data)throws Exception{
        cipher.init(Cipher.ENCRYPT_MODE,this.сryptKey,new IvParameterSpec(this.IV));
        return cipher.doFinal(data,0,data.length);
    }

    /**
     * File encryption operation
     * @param sourceFile file to be encrypted
     * @param destFile file to store encrypted data
     * @param password key phrase for secret key and IV generation
     * @throws Exception/ file doesn't exist or reading from file error
     */
    public void encryptFile(String sourceFile, String destFile, byte[] password) throws Exception{
        File srcFile=new File(sourceFile);
        if (!srcFile.exists()){
            throw new Exception("File doesn't exist");
        }
        if (!Files.exists(Paths.get(destFile))) {
            File newfile = new File(destFile);
            newfile.createNewFile();
        }
        byte[] hashedPassword = MessageDigest.getInstance("GOST3411_2012_512","JCP").digest(password);
        CryptParamsSpec params = CryptParamsSpec.getInstance();
        ru.CryptoPro.JCP.Key.SecretKeySpec spec= new ru.CryptoPro.JCP.Key.SecretKeySpec(Arrays.copyOf(hashedPassword,32), params);
        this.сryptKey = new GostSecretKey(spec);
        this.IV=Arrays.copyOfRange(hashedPassword,32,40);
        Files.write(Paths.get(destFile),encrypt(Files.readAllBytes(Paths.get(sourceFile))));
    }

    /**
     * Decryption operation
     * @param data - data to be decrypted
     * @return decrypted bytes
     * @throws Exception/ key or padding are invalid
     */
    public byte[] decrypt(byte[] data) throws Exception{
        cipher.init(Cipher.DECRYPT_MODE,this.сryptKey,new IvParameterSpec(this.IV));
        return cipher.doFinal(data,0,data.length);
    }

    /**
     * File decryption operation
     * @param sourceFile file to be decrypted
     * @param destFile file to store decrypted data
     * @param password key phrase for secret key and IV generation
     * @throws Exception/ file doesn't exist or reading from file error
     */
    public void decryptFile(String sourceFile, String destFile, byte[] password) throws Exception{
        File srcFile=new File(sourceFile);
        if (!srcFile.exists()){
            throw new Exception("File doesn't exist");
        }
        if (!Files.exists(Paths.get(destFile))) {
            File newfile = new File(destFile);
            newfile.createNewFile();
        }
        byte[] hashedPassword = MessageDigest.getInstance("GOST3411_2012_512","JCP").digest(password);
        CryptParamsSpec params = CryptParamsSpec.getInstance();
        ru.CryptoPro.JCP.Key.SecretKeySpec spec= new ru.CryptoPro.JCP.Key.SecretKeySpec(Arrays.copyOf(hashedPassword,32), params);
        this.сryptKey = new GostSecretKey(spec);
        this.IV=Arrays.copyOfRange(hashedPassword,32,40);
        Files.write(Paths.get(destFile),decrypt(Files.readAllBytes(Paths.get(sourceFile))));
    }

    /**
     * Message authentication code generation (not used)
     * @param k
     * @param data
     * @return
     * @throws NoSuchAlgorithmException
     * @throws NoSuchProviderException
     * @throws InvalidKeyException
     */
    public static byte[] getMAC(SecretKey k, byte[] data) throws NoSuchAlgorithmException,NoSuchProviderException,InvalidKeyException{
        Mac mac=Mac.getInstance("GOST28147", "Crypto");
        mac.init(k);
        mac.update(data);
        return mac.doFinal();
    }



}