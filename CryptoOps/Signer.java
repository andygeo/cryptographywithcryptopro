package CryptoOps;

import com.objsys.asn1j.runtime.Asn1Boolean;
import com.objsys.asn1j.runtime.Asn1ObjectIdentifier;
import com.objsys.asn1j.runtime.Asn1OctetString;
import ru.CryptoPro.JCP.ASN.PKIX1Explicit88.Extension;
import ru.CryptoPro.JCP.JCP;
import ru.CryptoPro.JCPRequest.GostCertificateRequest;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;



public class Signer {

    /**
     *  Digital sign (open key;private key) pair
     */
    private KeyPair keyPair;

    /**
     * Key generation algorithm
     */
    private String keyAlg=JCP.GOST_EL_2012_256_NAME;

    /**
     * Signature algorithm
     */
    private String signAlg=JCP.CRYPTOPRO_SIGN_2012_256_NAME;


    /**
     * Signature mode (hash-digest) size
     * True=512
     * False=256
     */
    private boolean mode=false;


    /**
     * Initialization mode
     */
    public static byte SIGN_MODE_256=0;    // generate 256-bit key
    public static byte SIGN_MODE_512=1;    // generate 512-bit key
    public static byte SIGN_MODE_NO_GEN=2; // without key pair generation




    public Signer() throws Exception{
        genKeyPair();
    }

    public Signer (byte mode) throws Exception{
        switch (mode){
            case 0: //SIGN_MODE_256
                break;
            case 1: //SIGN_MODE_512
                keyAlg=JCP.GOST_EL_2012_512_NAME;
                signAlg=JCP.CRYPTOPRO_SIGN_2012_512_NAME;
                this.mode=true;
                break;
            case 2: //SIGN_MODE_NO_GEN
                 return;
            default:throw new Exception("Wrong mode!");
        }
        genKeyPair();
    }

    /**
     * Generate key pair with specified by keyAlg type
     * @throws Exception/ if algorithm or provider doesn't exist
     */

    public void genKeyPair() throws Exception{
        final KeyPairGenerator keyGen = KeyPairGenerator.getInstance(keyAlg,"JCP");
        keyPair=keyGen.generateKeyPair();
    }

    /**
     * Signature generation
     * @param data  - data to be signed
     * @return byte array, containing the signature
     * @throws Exception /if algorithm or provider doesn't exist
     */
    public  byte[] sign(byte[] data) throws Exception {
        final Signature sig = Signature.getInstance(signAlg,"JCP");
        sig.initSign(keyPair.getPrivate());
        sig.update(data);
        return sig.sign();
    }



    /**
     *  Generate signature for file
     * @param filePath - path to the file to be signed
     * @return byte array, containing the signature
     * @throws Exception/if algorithm or provider doesn't exist
     */
    public byte[] signFile(String filePath) throws Exception {
        File file=new File(filePath);
        if(!file.exists()) {
            System.out.println("Wrong file name");
            throw new IllegalArgumentException("wrong file");
        }
        //IOException if readAllBytes not correct
        byte[] res=sign(Files.readAllBytes(Paths.get(filePath)));

        String signfile=filePath+".sgn";
        //System.out.println(signfile);
        if (!Files.exists(Paths.get(signfile))) {
            File newfile = new File(signfile);
            newfile.createNewFile();
        }
        BufferedWriter writer = new BufferedWriter(new FileWriter(signfile));
        writer.write(toHexString(res));
        writer.close();


        return res;
    }



    /**
     * Signature verification
     * @param data signed data
     * @param signature byte array, containing the signature
     * @return true if the signature is correct, false if it's not
     * @throws Exception /if algorithm or provider doesn't exist
     */
    public  boolean verify( byte[] data, byte[] signature) throws Exception {
        final Signature sig = Signature.getInstance(signAlg,"JCP");
        sig.initVerify(keyPair.getPublic());
        sig.update(data);
        return sig.verify(signature);
    }


    /**
     * Verification of a file signature
     * @param filePath - path to the file
     * @param signature - byte array, containing the signature
     * @return true if the signature is correct, false if it's not
     * @throws Exception/if algorithm or provider doesn't exist
     */
    public boolean verifyFile(String filePath, byte[] signature) throws Exception {
        File file=new File(filePath);
        if(!file.exists()) {
            System.out.println("Wrong file name");
            throw new IllegalArgumentException("wrong file");
        }
        //IOException if readAllBytes not correct
        return verify(Files.readAllBytes(Paths.get(filePath)),signature);
    }


    /**
     * Convert byte array to hex string
     * @param array - array of bytes to be converted
     * @return String representation of array in hex format
     */
    public static String toHexString(byte[] array) {
        final char[] hex = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A',
                'B', 'C', 'D', 'E', 'F'};
        StringBuffer ss = new StringBuffer(array.length * 3);
        for (int i = 0; i < array.length; i++) {
            ss.append(hex[(array[i] >>> 4) & 0xf]);
            ss.append(hex[array[i] & 0xf]);
        }
        return ss.toString();
    }


    /**
     * String with hex numbers conversion to byte array
     * (necessary to perform GOST examples, 2 string symbols=1 byte)
     * @param str - string to be converted
     * @return byte array, containing
     * @throws IllegalArgumentException when string contains non-hex symbol
     */
    public static byte[] toByteArr (String str) throws IllegalArgumentException{
        int size=str.length()/2;

        if (str.length()%2!=0) {
            size++;
            StringBuilder strB=new StringBuilder();
            strB.append("0");
            strB.append(str);
            str=strB.toString();
        }
        byte[] block=new byte [size];
        for (int i = 0; i <size; i++) {
            try{
                short buf=Short.parseShort(str.substring(2*i,2*i+2),16);
                block[i]=(byte)(buf&0xFF);
            } catch (NumberFormatException ex){//not a hex symbol in str
                throw new IllegalArgumentException();
            }

        }
        return block;
    }


    /**
     *  Save keyPair to the local cryptoPRO storage
     * @param alias -  id
     * @param password - key access password
     * @throws Exception/  wrong certificate/dnName format, certificate generation errors occurred
     */
    public void saveKeyPair(String alias, char[] password)throws Exception{
        String dnName,signAlgorithm;
        if (this.mode) {
            dnName="CN=newCert_2012_512, O=CryptoPro, C=RU";
            signAlgorithm=JCP.GOST_SIGN_2012_512_NAME;
        }
        else {
            dnName="CN=newCert_2012_256, O=CryptoPro, C=RU";
            signAlgorithm=JCP.GOST_SIGN_2012_256_NAME;
        }
        Certificate cert;
            try{
            byte[] encoded = createRequestAndGetCert(keyPair, signAlgorithm, JCP.PROVIDER_NAME, dnName);
            CertificateFactory cf=CertificateFactory.getInstance("X509");
             cert =cf.generateCertificate(new ByteArrayInputStream(encoded));
            } catch (Exception ex){ // no connection with CA or error during request
                final GostCertificateRequest gr = new GostCertificateRequest();
                // DER-encoded self-signed certificate generation
                final byte[] enc = gr.getEncodedSelfCert(keyPair, dnName);
                //  X509-certificate generation
                final CertificateFactory cf =
                        CertificateFactory.getInstance("X509");
                cert=cf.generateCertificate(new ByteArrayInputStream(enc));
            }
        KeyStore ks=KeyStore.getInstance("HDImageStore", "JCP");
        ks.load(null, null);
        Certificate[] certArr=new Certificate[1];
        certArr[0]=cert;
        ks.setKeyEntry(alias,keyPair.getPrivate(),password,certArr);
    }

    /**
     * Set certificate authority
     */
    public static boolean CERT_MODE_SELF=false; // self-signed certificate
    public static boolean CERT_MODE_CA=true;    // cryptoPRO-signed certificate

    /**
     *
     * CA http-address
     */
    public static final String HTTP_ADDRESS = "http://www.cryptopro.ru/certsrv/";

    /**
     * Create a request for certificate from given CA
     *
     * @param pair - keyPair (open key goes to the certificate
     *                        private one is used for request signing)
     * @param signAlgorithm sign algorithm
     * @param signatureProvider provider
     * @param dnName certificate DN-name
     * @return DER-encoded certificate
     * @throws Exception any errors occurred
     */
    public static byte[] createRequestAndGetCert(KeyPair pair, String signAlgorithm,
                                                 String signatureProvider, String dnName) throws Exception {

        GostCertificateRequest request = createRequest(pair,
                signAlgorithm, signatureProvider, dnName);
        return request.getEncodedCert(HTTP_ADDRESS);
    }


    /**
     * Функция формирует запрос на сертификат.
     *
     * @param pair ключевая пара. Открытый ключ попадает в запрос на сертификат,
     * секретный ключ для подписи запроса.
     * @param signAlgorithm Алгоритм подписи.
     * @param signatureProvider Провайдер подписи.
     * @param dnName DN-имя сертификата.
     * @return запрос
     * @throws Exception errors
     */
    public static GostCertificateRequest createRequest(KeyPair pair, String signAlgorithm,
                                                       String signatureProvider, String dnName) throws Exception {

        GostCertificateRequest request = new GostCertificateRequest(signatureProvider);


        /*
        Set key algorithm and certificate purposes
        */
        final String keyAlgorithm = pair.getPrivate().getAlgorithm();
        if (keyAlgorithm.equalsIgnoreCase(JCP.GOST_EL_DEGREE_NAME) ||
                keyAlgorithm.equalsIgnoreCase(JCP.GOST_EL_2012_256_NAME) ||
                keyAlgorithm.equalsIgnoreCase(JCP.GOST_EL_2012_512_NAME)) {
            int keyUsage = GostCertificateRequest.DIGITAL_SIGNATURE |
                    GostCertificateRequest.NON_REPUDIATION;
            request.setKeyUsage(keyUsage);
        }
        else {
            int keyUsage = GostCertificateRequest.DIGITAL_SIGNATURE |
                    GostCertificateRequest.NON_REPUDIATION |
                    GostCertificateRequest.KEY_ENCIPHERMENT |
                    GostCertificateRequest.KEY_AGREEMENT;
            request.setKeyUsage(keyUsage);
        }
        request.addExtKeyUsage(GostCertificateRequest.INTS_PKIX_EMAIL_PROTECTION);
        //certificate OID
        request.addExtKeyUsage("1.3.6.1.5.5.7.3.3");
        /*
        X509 v.3 certificates, signed by CA, should have
        Basic Constraints extension
         */
        Extension ext = new Extension();
        int[] extOid = {2, 5, 29, 19};
        ext.extnID = new Asn1ObjectIdentifier(extOid);
        ext.critical = new Asn1Boolean(true);
        byte[] extValue = {48, 6, 1, 1, -1, 2, 1, 5};
        ext.extnValue = new Asn1OctetString(extValue);
        request.addExtension(ext);

        // set public key to be added into certificate
        request.setPublicKeyInfo(pair.getPublic());
        // request subject name
        request.setSubjectInfo(dnName);
        // certificate signing and request encoding
        request.encodeAndSign(pair.getPrivate(), signAlgorithm);

        return request;
    }



    /**
     * Self-signed certificate generation
     * @return self-signed certificate
     * @throws Exception / keyPair or certificate standard isn't correct
     */
    public Certificate genSelfCert()
            throws Exception {
        final GostCertificateRequest gr = new GostCertificateRequest();
        String dnName;
        if (mode) dnName="CN=newCert_2012_512, O=CryptoPro, C=RU";
        else dnName="CN=newCert_2012_256, O=CryptoPro, C=RU";
        final byte[] enc = gr.getEncodedSelfCert(keyPair, dnName);
        final CertificateFactory cf =
                CertificateFactory.getInstance("X509");
        return cf.generateCertificate(new ByteArrayInputStream(enc));
    }


    /**
     * Load key pair from local cryptoPRO storage
     * @param alias - key id
     * @param password - key access password
     * @throws Exception/ wrong certificate format
     */
    public void loadKeyPair(String alias,char[] password) throws Exception{
        KeyStore ks=KeyStore.getInstance("HDImageStore", "JCP");
        ks.load(null, null);
        PrivateKey k=(PrivateKey)ks.getKey(alias,password);
        switch (k.getAlgorithm()){
            case "GOST3410_2012_256":
                this.mode=false;
                keyAlg=JCP.GOST_EL_2012_256_NAME;
                signAlg=JCP.CRYPTOPRO_SIGN_2012_256_NAME;
                break;
            case "GOST3410_2012_512":
                this.mode=true;
                keyAlg=JCP.GOST_EL_2012_512_NAME;
                signAlg=JCP.CRYPTOPRO_SIGN_2012_512_NAME;
                break;
            default: throw new Exception("Illegal signing algorithm!");
        }
        keyPair=new KeyPair(ks.getCertificate(alias).getPublicKey(),k);
    }
}
